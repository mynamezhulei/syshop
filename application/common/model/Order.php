<?php

namespace app\common\model;

use think\Model;

class Order extends BaseModel{

    /**
     * 定义订单的商品关联
     * order表的goods_id对应goods表的id
     */
    public function order_to_goods(){

    	return $this->hasMany('OrderToGoods');

    }

    /**
     * 定义订单的用户关联
     * order表的user_id对应user表的id
     */
    public function user(){

        return $this->belongsTo('user');

    }

    public function add($data){
        $data['status'] = 1;
        // $data['create_time'] = time();
        return $this->save($data);
    }
}
