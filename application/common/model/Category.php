<?php
namespace app\common\model;

use think\Model;

class Category extends BaseModel{

    /**
     * 获取一级分类
     */
    public function getFirstCategory($parentId=0){
        $where = [
            ['parent_id', '=', $parentId],
            ['status', '<>', '-1'],
        ];
        $order = [
            'id' => 'asc'
        ];
        $res = $this->where($where)->order($order)->paginate();
        // echo $this->getLastSql();
        return $res;
    }
}
