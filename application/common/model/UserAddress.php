<?php

namespace app\common\model;

use think\Model;

class UserAddress extends BaseModel{
	/**
     * 用户表关联user_address表
     */
    public function user(){

        $this->belongsTo('user');
    }

}