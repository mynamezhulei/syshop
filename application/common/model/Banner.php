<?php
namespace app\common\model;
use think\Model;

class Banner extends BaseModel{

    public function getbannerList(){
    	
        $where[] = ['status', 'neq', '-1'];
        $order = ['id'=>'desc']; 

        $res = $this->where($where)->order($order)->paginate();

        return $res;
    }


}