<?php
namespace app\common\model;

use think\Model;

class User extends BaseModel{

    /**
     * 用户表关联user_address表
     */
    public function UserAddress(){

        return $this->hasOne('UserAddress');
    }

    public function add($data=[]){
        if (!is_array($data)) {
            exception("传递的数据不是数组!");
        }
        $data['status'] = 1;
        
        $this->isUpdate(false)->allowField(true)->save($data);

        return $this->id;
    }

    //根据用户名获取用户信息
    public function getUserByMobile($mobile){
        if (!$mobile) {
            exception("请输入手机号码!");
        }
        $where = ['mobile'=>$mobile,'status'=>1];
        return $this->where($where)->find();
    }
}
