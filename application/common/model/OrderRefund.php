<?php

namespace app\common\model;

use think\Model;

class OrderRefund extends BaseModel{

    /**
     * 退款订单关联订单表
     * order_refund表的order_id对应order表的id
     */
    public function order(){

    	return $this->belongsTo('order');

    }

    public function add($data){
        $data['status'] = 1;
        // $data['create_time'] = time();
        return $this->save($data);
    }
}
