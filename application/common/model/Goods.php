<?php
namespace app\common\model;
use think\Model;

class Goods extends BaseModel{

	/**
     * 定义商品类目关联
     * goods表的categroy_id关联categroy表主键
     */
    public function category(){
        return $this->belongsTo('Category');
    }

    /**
     * 查询商品列表
     */
    public function getGoodsList(){
        $where = [
            ['status', 'neq', '-1']
        ];
        $order = ['status'=>'asc','id'=>'desc']; 

        $res = $this->where($where)->order($order)->paginate();
        // halt($res->toArray());
        // halt($this->getLastSql());
        return $res;
    }


}