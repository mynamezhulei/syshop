<?php

namespace app\common\model;

use think\Model;

class OrderToGoods extends BaseModel{

    /**
     * 定义订单的商品关联
     * order_to_g表的goods_id对应goods表的id
     */
    public function goods(){

    	return $this->belongsTo('goods');

    }

    /**
     * 定义订单的用户关联
     * order表的user_id对应user表的id
     */
    public function user(){

        return $this->belongsTo('user');

    }

    public function add($data){
        $data['status'] = 1;
        // $data['create_time'] = time();
        return $this->save($data);
    }
}
