<?php
namespace app\admin\controller;

class UserAddress extends Base{
	
	/**
	 * 用户列表
	 */
	public function list(){

		$list =  model('user')->where('status', '=', '1')->select();
		// halt($list[0]->UserAddress);
		return $this->fetch('',['list'=>$list]);
	}

	/**
	 * 删除用户列表
	 */
	public function delList(){
		
		return $this->fetch();
	}

	/**
	 * 用户积分列表
	 */
	public function scoreOperation(){
		
		return $this->fetch();
	}
}