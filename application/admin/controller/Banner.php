<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use Map;

class Banner extends Base{

    /**
     * 轮播图首页
     */
    public function index(){
        $banners = model('banner')->getbannerList();
        // halt($banners->toArray());
        return $this->fetch('', ['banners' => $banners]);
    }

}
