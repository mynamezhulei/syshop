<?php
namespace app\admin\controller;

class AdminUser extends Base{
	
	/**
	 * 管理员列表
	 */
	public function adminList(){

		$list = model('AdminUser')->where('status','<>','-1')->select();
        // halt($list);
		return $this->fetch('',['list'=>$list]);
	}

    /**
     * 管理员数据新增或更新
     */
    public function detail(){

        $data = request()->except('id');
        $id = request()->only('id');

        //获取控制器，而控制器名对应着model名
        $model = request()->controller();

        if(request()->isPost()){
        	//对密码单独修改
	        $salt = config('salt');
	        $data['passwd'] = md5($data['passwd'].$salt);
            //修改数据
            if(!empty($id)){
                $res = model($model)->updateById($data, $id);
                if ($res) {
                    $this->success("更新状态成功!");
                } else {
                    $this->error("更新状态失败!");
                }
            }else{
            //新增数据
                $id = model($model)->allowField(true)->add($data);
                // halt(model($model)->getLastSql());
                if ($id) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }

            }
        }else{
            if(!empty($id)){
                //修改数据详情展示
                $detail = model($model)->where($id)->where('status', 'neq','-1')->find();
                // halt(Db::name($model)->getLastSql());
            }else{
                $detail = [];
            }
            
            // halt($detail);
            return $this->fetch('',['detail'=>$detail]);
        }
        
    }

	/**
	 * 角色管理
	 */
	public function adminRole(){
		
		return $this->fetch();
	}

	/**
	 * 角色添加
	 */
	public function adminRoleAdd(){

		return $this->fetch();
	}
	/**
	 * 权限管理
	 */
	public function adminPermission(){
		
		return $this->fetch();
	}
}