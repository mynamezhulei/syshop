<?php
namespace app\admin\controller;

class Goods extends Base{
    /**
     * 商品首页
     */
    public function index(){
        $goods = model('goods')->getGoodsList();
        $categorys = model('category')->getFirstCategory();
        // halt($goods->toArray());
        return $this->fetch('', [
        	'goods' => $goods,
        	'categorys' => $categorys
        ]);
    }

    /**
     * 商品详情和修改
     */
    public function detail(){
        ini_set('max_execution_time', '180');
        ini_set('max_input_time','90');
        ini_set('post_max_size', '12M');
        ini_set('upload_max_filesize','10M'); 
        ini_set('memory_limit','20M');

        $data = request()->except('id');
        $id = request()->only('id');

        //获取控制器，而控制器名对应着model名
        $model = request()->controller();

        if(request()->isPost()){
            //修改数据
            if(!empty($id)){
                $res = model($model)->updateById($data, $id);
                if ($res) {
                    $this->success("更新状态成功!");
                } else {
                    $this->error("更新状态失败!");
                }
            }else{
            //新增数据
                $id = model($model)->allowField(true)->add($data);
                // halt(model($model)->getLastSql());
                if ($id) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }

            }
        }else{
            if(!empty($id)){
                //修改数据详情展示
                $detail = model($model)->where($id)->where('status','neq','-1')->find();
                // halt(Db::name($model)->getLastSql());
            }else{
                
                $detail = [];
            }

            $categorys = model('category')->getFirstCategory();
            
            // halt($detail->toArray());
            return $this->fetch('',[
                'detail'=>$detail,
                'categorys' => $categorys
            ]);
        }
        
    }

}

