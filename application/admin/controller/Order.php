<?php
namespace app\admin\controller;

class Order extends Base{
	
	/**
	 * 代发货订单
	 */
	public function ListToShip(){
		$list = model('order')->where(['status'=>0])->paginate();
		// halt(model('order')->getLastSql());
		return $this->fetch('',['list' => $list]);
	}

	/**
	 * 代退款订单
	 */
	public function ListToRefund(){
		$list = model('orderRefund')->where(['status'=>0])->paginate();
		// halt(model('orderRefund')->getLastSql());
		return $this->fetch('',['list' => $list]);
	}

	/**
	 * 所有订单
	 */
	public function list(){
		$list = model('order')->paginate();
		return $this->fetch('',['list' => $list]);
	}

}