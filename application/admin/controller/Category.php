<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use Map;

class Category extends Base
{
    //显示分类列表
    public function index()
    {
        $parentId = input('get.parent_id', 0, 'intval');
        $categorys = model('Category')->getFirstCategory($parentId);
        // halt($categorys->toArray());
        return $this->fetch('', [
            'categorys' => $categorys,
        ]);
    }

    public function listorder($id, $listorder)
    {
        $res = model('Category')->save(['listorder'=>$listorder], ['id'=>$id]);
        if ($res) {
            $this->result($_SERVER['HTTP_REFERER'], 200, 'ok');
        } else {
            $this->result($_SERVER['HTTP_REFERER'], 400, 'fail');
        }
    }

}
