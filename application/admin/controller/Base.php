<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Base extends Controller{
    /**
     * 初始化方法
     */
    public function _initialize(){
        // 判断用户是否登录
        if(strtolower(request()->controller()) !== 'base' && !session('o2o_admin_user')){
            $this->redirect('base/login');
        }
    }

    public function login(){
        if(request()->isPost()){    
            $data = input("post.");
            //TODO 验证
            try {
                $user = model("AdminUser")->where('user_name','=',$data['username'])->find();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            if (!$user) {
                $this->error("该用户不存在!");
            }
            $salt = config('salt');
            if (md5($data['password'].$salt)!==$user['passwd']) {
                $this->error("密码错误!");
            }

            //登录成功
            $login_data['last_login_ip'] = get_client_ip();
            $login_data['last_login_time'] = date('Y-m-d H:i:s');
            model("user")->updateById($login_data, ['id'=>$user['id']]);            

            session('o2o_admin_user',$user->toArray());
            $this->success("登录成功!", url("index/index"));
        }else{
            return $this->fetch();
        }
    }

    public function logout(){
        session('o2o_admin_user', NULL);
        $this->redirect("base/login");
    }

    /**
     * 通用修改状态
     */
    public function status()
    {
        $data = input('post.');
        //TODO 验证
        if (empty($data['id'])) {
            $this->error("id不合法!");
        }
        if (!is_numeric($data['status'])) {
            $this->error("status不合法!");
        }
        //获取控制器，而控制器名对应着model名
        $model = request()->controller();
        $res = model($model)->save(['status'=>$data['status']], ['id'=>$data['id']]);
        return $res;
    }

    /**
     * 通用数据新增或更新
     */
    public function detail(){

        $data = request()->except('id');
        $id = request()->only('id');

        //获取控制器，而控制器名对应着model名
        $model = request()->controller();

        if(request()->isPost()){
            //修改数据
            if(!empty($id)){
                $res = model($model)->updateById($data, $id);
                if ($res) {
                    $this->success("更新状态成功!");
                } else {
                    $this->error("更新状态失败!");
                }
            }else{
            //新增数据
                $id = model($model)->allowField(true)->add($data);
                // halt(model($model)->getLastSql());
                if ($id) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }

            }
        }else{
            if(!empty($id)){
                //修改数据详情展示
                $detail = model($model)->where($id)->where('status','neq','-1')->find();
                // halt(Db::name($model)->getLastSql());
            }else{
                $detail = [];
            }

            // halt($detail);
            return $this->fetch('',['detail'=>$detail]);
        }
        
    }

}
