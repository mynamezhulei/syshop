<?php
namespace app\index\controller;

use think\Controller;

/**
 * 前台商品
 */
class Goods extends Base{

    /**
     * 前台分类展示商品
     */
    public function category(){
        $category_id = input('get.category_id');
        $category_name = input('get.category_name');
        $category_image = input('get.category_image');
        // halt($category_image);
        $list = model('goods')->where('category_id','=',$category_id)->where('status','=',1)->select();
        return $this->fetch('',[
            'list'=>$list,
            'category_id' => $category_id,
            'category_name' => $category_name,
            'category_image' => $category_image
        ]);
    }

    /**
     * 前台商品详情
     */
    public function detail(){
        
        $id = input('get.id');
        //根据id查询商品数据
        $detail =model("goods")->where(['id'=>$id])->find();
        // halt(model("goods")->getLastSql());
        return $this->fetch('',[
            'detail' => $detail
        ]);
    }


}
