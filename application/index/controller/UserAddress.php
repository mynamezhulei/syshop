<?php
namespace app\index\controller;

use think\Controller;

class UserAddress extends Base{


    /**
     * 添加或修改收货人地址
     */
    public function detail(){
        return $this->fetch();
    }

    /**
     * 收货人地址列表
     */
    public function addressList(){
        return $this->fetch();
    }

}
