<?php
namespace app\index\controller;

use app\api\controller\Wechat;

class User extends Base{

    public function login(){

        $user = session('o2o_user');
        if ($user) {
            $this->redirect('index/index');
        }
        return $this->fetch();
    }

    public function register(){

        if (request()->isPost()) {
            $data = input("post.");
            if (captcha_check($data['verifyCode'])) {
                if ($data['password']!==$data['repassword']) {
                    $this->error("两次密码不一致");
                }
                $data['code'] = mt_rand(100, 10000);
                $data['password'] = md5($data['password'].$data['code']);
                try {
                    //user表中mobile和email为唯一索引，若传入的值重复则会抛出异常
                    $res = model("user")->add($data);
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                }
                if ($res) {
                    $this->success("注册成功!", url("user/login"));
                } else {
                    $this->error("注册失败!");
                }
            } else {
                return $this->error("验证码错误!");
            }
        } else {
            return $this->fetch();
        }
    }

    /**
     * 微信登陆
     */
    public function WechatLogin(){

        ini_set('session.gc_maxlifetime', "7200"); // 秒
        
        $app_id = config('wechat.app_id');
        $app_secrect = config('wechat.app_secrect');
        $code = input('get.code');

        $wechat = new Wechat();
        //获取access_token
        $result = $wechat->getAccessToken($app_id, $app_secrect, $code);
        $result = json_decode($result);
        //获取用户信息
        $user_info = $wechat->getUserinfo($result['openid'], $result['access_token']);
        $user_info = json_decode($user_info);
        //对微信用户信息返回值转换_
        $user_info['union_id'] = $user_info['unionid'];
        $user_info['open_id'] = $user_info['openid'];
        $user_info['username'] = $user_info['nickname'];

        //判断是否注册
        $is_register = model('user')->where('union_id', '=', $user_info['union_id'])->find();
        if(!$is_register){
            //写入用户信息
            $user_id = model('user')->add($user_info);
            $user_info = array_merge(['user_id'=>$user_id], $user_info);
            session('o2o_user', $user_info);
        }else{
            $this->redirect('index/index');
        }


        
    }

    public function logincheck(){

        if (!request()->isPost()) {
            $this->error("提交不合法!");
        }
        $data = input("post.");
        //TODO 验证
        try {
            $user = model("user")->getUserByMobile($data['mobile']);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if (!$user) {
            $this->error("该用户不存在!");
        }
        if (md5($data['password'].$user->code)!==$user->password) {
            $this->error("密码错误!");
        }
        //登录成功
        model("user")->updateById(['last_login_time'=>date('Y-m-d H:i:s')], ['id'=>$user->id]);

        //记录用户信息到session
        session('o2o_user', $user->toArray());
        $this->success("登录成功!", url("index/index"));
    }

    public function logout(){

        session('o2o_user', null);
        $this->redirect("user/login");
    }

    /**
     * 个人中心
     */
    public function personal(){

        return $this->fetch();
    }


}
