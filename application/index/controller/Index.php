<?php
namespace app\index\controller;

use think\Controller;

class Index extends Base{   
    /**
     * 前台首页
     */
    public function index(){

        $banners = model('banner')->where('status','=',1)->select();
        //获取四个子分类
        $category = model('category')->where('status','=',1)->select();
        //获取首页商品
        $goods = model("goods")->where('status','=',1)->limit(10)->select();

        return $this->fetch('', [
            "banners"   => $banners,
            'category'   => $category,
            'goods'      => $goods
        ]);
    }

}
