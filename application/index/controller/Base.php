<?php
namespace app\index\controller;

use think\Controller;

class Base extends Controller{
    /**
     * 初始化加载
     */
    public function initialize(){
        $controller = strtolower(request()->controller());
        $user = session('o2o_user');
        if ($controller !== 'user' && !$user) {
            $this->redirect('user/login');
        }
        $this->assign('controller', $controller);
        $this->assign('title', '团购首页');
    }

    //获取首页推荐商品分类信息
    public function getRecommendCats()
    {
        $parentIds=$sedcatArr=$recomCats = [];
        $cats = model("category")->getNormalRecommendCategoryByParentId(0, 5);
        //获取一级分类id
        foreach ($cats as $cat) {
            $parentIds[] = $cat->id;
        }
        //获取二级分类数据
        $sedCats = model("category")->getNormalCategoryIdParentId($parentIds);
        foreach ($sedCats as $sedcat) {
            $sedcatArr[$sedcat->parent_id][] = array(
               'id'   => $sedcat->id,
               'name' => $sedcat->name
           );
        }
        foreach ($cats as $cat) {
            //recomCats为一级和二级分类的所有数据，第一个参数是一级分类的name，第二个参数是该一级分类下的所有二级分类数据
            $recomCats[$cat->id] = [
               $cat->name,empty($sedcatArr[$cat->id])?[]:$sedcatArr[$cat->id]
           ];
        }
        return $recomCats;
    }

    /**
     * 通用数据新增或更新
     */
    public function detail(){

        $data = request()->except('id');
        $id = request()->only('id');

        //获取控制器，而控制器名对应着model名
        $model = request()->controller();

        if(request()->isPost()){
            //修改数据
            if(!empty($id)){
                $res = model($model)->updateById($data, $id);
                if ($res) {
                    $this->success("更新状态成功!");
                } else {
                    $this->error("更新状态失败!");
                }
            }else{
            //新增数据
                $id = model($model)->allowField(true)->add($data);
                // halt(model($model)->getLastSql());
                if ($id) {
                    $this->success('添加成功');
                } else {
                    $this->error('添加失败');
                }

            }
        }else{
            if(!empty($id)){
                //修改数据详情展示
                $detail = model($model)->where($id)->where(['status' =>['neq','-1']])->find();
                // halt(Db::name($model)->getLastSql());
            }else{
                $detail = [];
            }
            
            // halt($detail);
            return $this->fetch('',['detail'=>$detail]);
        }
        
    }
}
