<?php
namespace app\index\controller;

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

class Order extends Base{
    /**
     * 订单列表
     */
    public function list(){
        
        return $this->fetch('');
    }

    public function index(){
        $user = session('o2o_user');
        if (!$user['mobile']) {
            $this->redirect('user/detail');
        }
        $id = input('get.id', 0, 'intval');
        if (!$id) {
            $this->error("参数不合法!");
        }
        $goodsCount = input('get.goods_count', 1, 'intval');
        $totalPrice = input('get.total_price', 0, 'intval');
        $goods = model('goods')->find($id);
        if (!$goods || $goods->status!=1) {
            $this->error("商品不存在!");
        }
        if (empty($_SERVER['HTTP_REFERER'])) {
            $this->error("请求不合法!");
        }
        //入库
        $orderSn = setOrderSn();    //获取订单号
        $data = array(
            'trade_no'   => $orderSn,
            'user_id'        => $user['id'],
            'total_price'    => $totalPrice,
            'client_ip'        => get_client_ip(),
            'deliver_address' => 1
        );
        //数据库插入更新等一般都需要try catch处理
        try {
            $orderId = model('order')->add($data);
            $order_to_goods =[
                ['goods_id'        => $goods['id'],
                'num'     => $goodsCount,
                'goods_price' => $goods['price'],
                'order_id' => $orderId
                ]
            ];
            model('OrderToGoods')->saveAll($order_to_goods);
        } catch (\Exception $e) {
            // halt($e->getMessage());
            $this->error("订单处理失败!");
        }
       
        $this->redirect(url('pay/index', ['id'=>$orderId]));
    }


    public function confirm(){
        $user = session('o2o_user');
        if (!$user['mobile']) {
            $this->redirect('user/detail');
        }
        $id = input('get.id', 0, 'intval');
        if (!$id) {
            $this->error("参数不合法!");
        }
        $count = input('get.count', 1, 'intval');
        $goods = model('goods')->where('status',1)->find($id);
        // halt(model('goods')->getLastSql());
        if (!$goods) {
            $this->error("商品不存在!");
        }
        $goods = $goods->toArray();
       
        return $this->fetch('', [
            'goods'     => $goods,
            'count'    => $count,
        ]);
    }

    public function wxPay(){
        $orderNo = time() . rand(1000, 9999);
        $wxConfig = config('wxpay');
        // 订单信息
        $payData = [
            'body'    => 'test body',
            'subject'    => 'test subject',
            'order_no'    => $orderNo,
            'timeout_express' => time() + 600,// 表示必须 600s 内付款
            'amount'    => '3.01',// 微信沙箱模式，需要金额固定为3.01
            'return_param' => '123',
            'client_ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',// 客户地址
            'openid' => 'ottkCuO1PW1Dnh6PWFffNk-2MPbY',
            'product_id' => '123',

            // 如果是服务商，请提供以下参数
            'sub_appid' => '',//微信分配的子商户公众账号ID
            'sub_mch_id' => '',// 微信支付分配的子商户号
            'sub_openid' => '',// 用户在子商户appid下的唯一标识
        ];

        try {
            $ret = Charge::run(Config::WX_CHANNEL_PUB, $wxConfig, $payData);
        } catch (PayException $e) {
            echo $e->errorMessage();
            exit;
        }

        echo json_encode($ret, JSON_UNESCAPED_UNICODE);
    }

}
