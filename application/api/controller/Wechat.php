<?php

namespace app\api\controller;

use think\Controller;

class Wechat extends Controller{

    /**
     * 获取access_token
     * 成功   { "access_token":"ACCESS_TOKEN",
                "expires_in":7200,
                "refresh_token":"REFRESH_TOKEN",
                "openid":"OPENID",
                "scope":"SCOPE" }
        失败  {"errcode":40029,"errmsg":"invalid code"}
     */
    public function getAccessToken($app_id, $secret, $code) {

        $url = "https://api.weixin.qq.com/sns/oauth2/access_token";
        $param = "appid=APPID&secret=$secret&code=$code&grant_type=authorization_code";
        $result = sendCurl($url, $param);
        return $result;
    }

    /**
     * 检验授权凭证（access_token）是否有效
     * 成功   { "errcode":0,"errmsg":"ok"}
     * 失败   { "errcode":40003,"errmsg":"invalid openid"}
     */
    public function chechAccessToken($open_id, $access_token){

        $url = "https://api.weixin.qq.com/sns/auth";
        $param = "access_token=$access_token&openid=open_id";
        $result = sendCurl($url, $param);
        return $result;
    }

    /**
     * 刷新token
     * refresh_token有效期为30天
     * 成功   { "access_token":"ACCESS_TOKEN",
                "expires_in":7200,
                "refresh_token":"REFRESH_TOKEN",
                "openid":"OPENID",
                "scope":"SCOPE" }
     */
    public function refreshToken($app_id, $refesh_token){

        $url = "https://api.weixin.qq.com/sns/oauth2/refresh_token";
        $param = "appid=$app_id&grant_type=refresh_token&refresh_token=$refesh_token";
        $result = sendCurl($url, $param);
        return $result;
    }

    /**
     * 微信获取用户基本信息（UnionID机制）
     *
     * 成功   { "openid":" OPENID",
                "nickname": NICKNAME,
                "sex":"1",
                "province":"PROVINCE"
                "city":"CITY",
                "country":"COUNTRY",
                "headimgurl": "http://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqe/46",
                "privilege":[ "PRIVILEGE1" "PRIVILEGE2"     ],
                "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
            }
     */
    public function getUserinfo($open_id, $access_token) {
        if ($access_token && $openid) {
            $url = "https://api.weixin.qq.com/sns/userinfo";
            $param = "access_token=$access_token&openid=$open_id&lang=zh_CN";
            $userinfo = sendCurl($url, $param);
            return $userinfo;
        } else {
            return array("code" => "userinfo_null");
        }
    }



}

