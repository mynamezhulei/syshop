<?php
namespace app\api\controller;

use think\{Controller, Request, File};

class Image extends Controller{

    public function upload(){

        $file = request()->file('file');
        //图片上传目录
        $info = $file->move('upload');

        return show(201, 'ok', '/'.$info->getPathname());
    }

    /**
     * wangEditor文件上传
     */
    public function wangEditor_upload() {

        $file = request()->file('file');
        //图片上传目录
        $info = $file->move('upload');
   

        //执行上传任务  
        if ($info) {
            $result['errno'] = 0;
            $result['data'][] = '/'.$info->getPathname();
            return json($result);
        } else {
            $result['errno'] = -1;
            return  json($result);
        }
    }

}
