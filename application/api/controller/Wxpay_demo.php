<?php

namespace app\api\controller;
use Think\Db;
use think\Model;
use app\api\model\Order;
use app\api\model\User;

class Wxpay extends Base{

    public function test()
    {
        // 测试数据接口
        $list = explode(',', '2,3');
        dump(count($list));
        dump(config('APP_APPID'));
        dump($this->spbill_create_ip = $_SERVER["REMOTE_ADDR"]); 
    }
    /**
     * 公众号支付调用统一下单支付接口
     */
    public function getPrepayIds(){
            // 接受web端传递过来的参数
            $body = $this->param['goods_name'];
            //$body = '一分钱商品';
            $body = $this->strFilter($body);
            $total_fee = floatval($this->param['charge'])*100;
            //$total_fee = 1;
            $ordernumbers = $this->param['order_num'];
            //$ordernumbers = '1238569852';

            $notify_url = 'http://ll.xdjst.com/api/Weiapppay/getResult';
            $appid = config('WXAPP_APPID');
            $secret = config('WXAPP_APPSECRET');
            $mch_id = config('WXAPP_MCHID');
            $key = config('WXAPP_KEY');
            // 1，创建支付对象
            $wechatAppPay = new WeChatAppPay($appid, $mch_id, $notify_url, $key);
            // 2，封装统一下单接口的参数
            $params['body'] = $body;         //格式：应用市场上的app名称 --- 商品描述
            $params['out_trade_no'] = $ordernumbers;    //###自定义的订单号(不可一样)
            $params['total_fee'] = $total_fee;     //***订单金额只能为整数单位为分
            $params['trade_type'] = 'JSAPI';       //交易类型 JSAPI | NATIVE | APP | WAP
            if(isset($this->param['openid'])){
                $params['openid'] = $this->param['openid'];
                // 3，调用统一下单方法获取prepay_id,创建手机app端预支付参数
                $data = $wechatAppPay->unifiedOrder($params);
                $data = $wechatAppPay->getAppPayParams2($data['prepay_id']);
            }else{
                // 3，调用统一下单方法获取prepay_id,创建手机app端预支付参数
                $data = $wechatAppPay->unifiedOrder($params);
                $data = $wechatAppPay->getAppPayParams($data['prepay_id']);
            }
            
            //$data['retcode'] = 0;
            //$data['retmsg'] = 'ok';
            //print_r($data);die(); //成功的情况下包含prepay_id
            //$data = json_encode($data);
            //$file = fopen('./FeiWXPay',"a+");
            //fwrite($file,$data);
            if ($data) {
            return json_encode($data);
            }else{
                $this->response('请求参数失败',201,['status'=>0]);
            }
    }

    /**
     * 去掉字符串中的特殊字符函数
     */
    public function strFilter($str){
        $str = str_replace('`', '', $str);
        $str = str_replace('·', '', $str);
        $str = str_replace('~', '', $str);
        $str = str_replace('!', '', $str);
        $str = str_replace('！', '', $str);
        $str = str_replace('@', '', $str);
        $str = str_replace('#', '', $str);
        $str = str_replace('$', '', $str);
        $str = str_replace('￥', '', $str);
        $str = str_replace('%', '', $str);
        $str = str_replace('^', '', $str);
        $str = str_replace('……', '', $str);
        $str = str_replace('&', '', $str);
        $str = str_replace('*', '', $str);
        $str = str_replace('(', '', $str);
        $str = str_replace(')', '', $str);
        $str = str_replace('（', '', $str);
        $str = str_replace('）', '', $str);
        $str = str_replace('-', '', $str);
        $str = str_replace('_', '', $str);
        $str = str_replace('——', '', $str);
        $str = str_replace('+', '', $str);
        $str = str_replace('=', '', $str);
        $str = str_replace('|', '', $str);
        $str = str_replace('\\', '', $str);
        $str = str_replace('[', '', $str);
        $str = str_replace(']', '', $str);
        $str = str_replace('【', '', $str);
        $str = str_replace('】', '', $str);
        $str = str_replace('{', '', $str);
        $str = str_replace('}', '', $str);
        $str = str_replace(';', '', $str);
        $str = str_replace('；', '', $str);
        $str = str_replace(':', '', $str);
        $str = str_replace('：', '', $str);
        $str = str_replace('\'', '', $str);
        $str = str_replace('"', '', $str);
        $str = str_replace('“', '', $str);
        $str = str_replace('”', '', $str);
        $str = str_replace(',', '', $str);
        $str = str_replace('，', '', $str);
        $str = str_replace('<', '', $str);
        $str = str_replace('>', '', $str);
        $str = str_replace('《', '', $str);
        $str = str_replace('》', '', $str);
        $str = str_replace('.', '', $str);
        $str = str_replace('。', '', $str);
        $str = str_replace('/', '', $str);
        $str = str_replace('、', '', $str);
        $str = str_replace('?', '', $str);
        $str = str_replace('？', '', $str);
        return trim($str);
    }

    public function getPrepay(){
        $data = M('order')->where(['master_order_sn'=>'201704211627074429'])->select();
        dump($data);exit;


        $data = I('wxdata');
        $this->ajaxReturn([
            //'status' => 1,
            //'msg' => '请求支付参数成功!',
            'data' => $data
        ],'jsonp');
    }

    /**
     * @return bool
     * 支付回调函数地址---接收异步通知
     */
    public function getResult(){
        $notify_url = 'http://ll.xdjst.com/api/Weiapppay/getResult';
        $appid = config('APP_APPID');
        $secret = config('APP_APPSECRET');
        $mch_id = config('APP_MCHID');
        $key = config('APP_KEY');
        $wechat = new WeChatAppPay($appid, $mch_id, $notify_url, $key);
        // 首先，获取到微信异步返回的支付通知数据
        $back_data = $wechat -> getNotifyData();
        
//        $textFile = fopen('./appPaylogg.txt','a+');
//        fwrite($textFile,$back_data.'|||'.time());
//        fclose($textFile);
        

        if($back_data){
            $wechat -> replyNotify();
        }
        // 重写回调函数---主动查询订单支付结果
        $result = $wechat -> NotifyProcess($back_data);
        if($result === false){
            $wechat -> replyNotify();
        }
        //  测试返回是否成功访问到支付回调函数---根据txt文件的大小，最好是每5天更换
        //  一个日志本
        $back_data = json_encode($back_data);
        $textFile = fopen('./appPayg.txt','a+');
        fwrite($textFile,$back_data.'|||'.time());
        fclose($textFile);
        $wechat -> replyNotify();
    }
    /**
    * 对象 转 数组
    * @param object $obj 对象
    * @return array
    */
    function objectToArray ($object) {  
        if(!is_object($object) && !is_array($object)) {  
            return $object;  
        }  

        return array_map('objectToArray', (array) $object);  
    } 

}
