<?php

namespace app\api\controller;

use Payment\Common\PayException;
use Payment\Client\{Charge, Notify};
use app\common\model\{Order,OrderRefund};
use think\{Controller,Log};
/**
 * 微信支付类
 */
class WechatPay extends Controller{

	/**
	 * 微信下单支付
	 */
	public function WechatPay(){
		$wxConfig = config('wechat');
		$orderNo = 'suyang'. time() . rand(1000, 9999);
		$goods_name = input('post.goods_name');
		$total_price = input('post.total_price');
		$user_id = session('o2o_user.user_id');
		$openid = session('o2o_user.open_id');
		// 订单信息
		$payData = [
		    'body'    => '苏扬商城1',
		    'subject'    => $goods_name,
		    'order_no'    => $orderNo,
		    'timeout_express' => time() + 600,// 表示必须 600s 内付款
		    'amount'    => '3.01',// 微信沙箱模式，需要金额固定为3.01
		    'return_param' => $user_id,
		    'client_ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',// 客户地址
		    'openid' => $openid,
		];

		try {
		    $ret = Charge::run('wx_pub', $wxConfig, $payData);
		} catch (PayException $e) {
		    return  $e->errorMessage();
		}

		return  json_encode($ret, JSON_UNESCAPED_UNICODE);
	}

	
	/**
	 * 微信支付回调接口 --接受异步通知
	 */
	public function WechatNotify(){
		$wxConfig = config('wechat');
		$type = 'wx_charge';

		Log::init([
		    'type'  =>  'File',
		    'path'  =>  '../runtime/paylog/'
		]);
		
		try {

    		$data = Notify::getNotifyData($type, $wxConfig);// 获取第三方的原始数据，未进行签名检查

    		Log::info("支付回调数据——".$data);

    		$order = Order::where('trade_no','=',$data['out_trade_no'])->find();

    		if($order){
    			if($order['status'] == 1){
    				return true;
	    		}
	    		if($order['status'] == 0 && $order['total_price'] == $data['total_fee']){
	    			$data[] = [
	    				'transaction_id' => $data['transaction_id'], //微信支付订单号
						'status' => 1,	// 0-未支付 1-已支付
						'payment_id' => 1, //1-微信支付
						'pay_status' => 1, //0-未支付 1-支付成功 2-支付失败
						'pay_time' =>date('Y-m-d H:i:s'),
	    			];
	    			Order::where('trade_no','=', 'out_trade_no')->update($data);
	    			return true;
	    		}
    		}
    		
    		// $ret = Notify::run($type, $config, $callback);// 处理回调，内部进行了签名检查
		} catch (PayException $e) {
		    Log::error("支付失败——".$e->errorMessage()) ;
		    return false;
		}
	}

    /**
     * 微信退款接口
     */
    public function WechatRefund(){
        $trade_no = input('trade_no');
        $refund_fee = input('refund_fee');

        $refundNo = time() . rand(1000, 9999);
        $wxConfig = config('wxpay');
        if (empty($trade_no) || empty($refund_fee)){
            return false;
        }
        $data = [
            'out_trade_no' => '14935385689468',
            'total_fee' => '3.01',
            'refund_fee' => '3.01',
            'refund_no' => $refundNo,
            'refund_account' => WxConfig::REFUND_RECHARGE,// REFUND_RECHARGE:可用余额退款  REFUND_UNSETTLED:未结算资金退款（默认）
        ];

        try {
            $ret = Refund::run(Config::WX_REFUND, $wxConfig, $data);
            if ($ret){
                OrderRefund::where(['trade_no' => $trade_no])->update(['status' => 1]);
                Order::where(['trade_no' => $trade_no])->update(['status' => 4]);
            }

        } catch (PayException $e) {

            return  $this->error($e->errorMessage());
        }

        return $this->success(json_encode($ret, JSON_UNESCAPED_UNICODE));

    }
}