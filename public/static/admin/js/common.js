function selecttime(flag){
    if(flag=='endTime'){
        var endTime = $("#countTimeend").val();
        if(endTime != ""){
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',maxDate:endTime})}else{
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})}
    }else{
        var startTime = $("#countTimestart").val();
        if(startTime != ""){
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',minDate:startTime})}else{
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})}
    }
 }

/*通用-发布*/
function general_start(url,data){
    console.log(data);
    layer.confirm('确认要发布吗？',function(index){
        var obj;
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function(data) {
                layer.msg('已发布!',{icon: 6,time:1000},function(){
                    window.location.reload();
                });
            },
            error:function(data) {
                console.log(data.msg);
            },
        });
    });
}

/*通用-下架*/
function general_stop(url,data){
    layer.confirm('确认要下架吗？',function(index){
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function(data){
                layer.msg('已下架!',{icon: 5,time:1000},function(){
                    window.location.reload();
                });
            },
            error:function(data) {
                console.log(data.msg);
            },
        }); 
        
    });
}

/*通用-编辑*/
function general_edit(title,url){
    var index = layer.open({
        type: 2,
        title: title,
        content: url
    });
    layer.full(index);
}

/*通用-删除*/
function general_del(url,data){
    layer.confirm('确认要删除吗？',function(index){
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data :data,
            success: function(data){
                layer.msg('已删除!',{icon:1,time:1000},function(){
                    window.location.reload();
                });
            },
            error:function(data) {
                console.log(data.msg);
            },
        });     
    });
}
