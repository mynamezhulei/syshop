$(function() {

    if(document.getElementById('thumbnail_upload')){
       $("#thumbnail_upload").uploadify({
            'swf'             : SCOPE.uploadify_swf,
            'uploader'        : SCOPE.image_upload,
            'buttonText'      : "图片上传",
            'fileTypeDesc'    : "Image files",
            'fileObjName'     : 'file',
            'fileTypeExts'    : '*.gif; *.jpg; *.png',
            'onUploadSuccess' : function(file, data, response) {
                if(response){
                    var obj = JSON.parse(data);
                    console.log(data);
                    $('#thumbnail_upload_preview').attr("src",obj.data);
                    $('#thumbnail_upload_preview').attr("style",'width:150px;height:150px');
                    $('#thumbnail_upload_preview').Huipreview();
                    $('#thumbnail').attr("value",obj.data);
                }
            }
        }); 
    }

    if(document.getElementById('file_upload')){
       $("#file_upload").uploadify({
            'swf'             : SCOPE.uploadify_swf,
            'uploader'        : SCOPE.image_upload,
            'buttonText'      : "图片上传",
            'fileTypeDesc'    : "Image files",
            'fileObjName'     : 'file',
            'fileTypeExts'    : '*.gif; *.jpg; *.png',
            'onUploadSuccess' : function(file, data, response) {
                if(response){
                    var obj = JSON.parse(data);
                    console.log(data);
                    var li='<li><img src="'+obj.data+'"><i class="btn-delete">×</i></li>'
                    $('#imageList').append(li);
                    $('#imageList img').Huipreview();
                    var value=$('#file_upload_image').val();
                    if(value==""){
                          $('#file_upload_image').val(obj.data);
                    }else{
                          $('#file_upload_image').val(value+"|"+obj.data);
                    }
                }
            },
            'uploadLimit'   : 5
        }); 
    }
    
    $("#imageList").on("click",".btn-delete",function(){
        var liItem=$(this).closest("li");
        var index=$(liItem).index();
        liItem.remove();
          var value=$('#file_upload_image').val();
        var dataList=value.split("|");
        dataList.splice(index,1);
        $('#file_upload_image').val(dataList.join("|"));
    })

       
});
