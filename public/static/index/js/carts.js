;
(function(){
    var totalPriceNumber = 0;      //最后结算价格

    http('http://192.168.1.112:3030/api/carts','GET').then(data=>{
        spliceHTML()(data.result.carts)
        sessionStorage.setItem('carts_data',JSON.stringify(data.result.carts))
    }).catch(err=>{
        console.log(err)
    })

    $('.cart_product_box').on('click','.reduce',function(event){          //减少商品数量
        event.stopPropagation()
        var price = getSinglePrice($(this).parents('.dada').index())
        var count = parseInt($(this).siblings('.count_num').text())
        if(count === 1){
            count = 1
        }else {
            count--
            if($(this).parents('.price_box').siblings('.products').find('.round_btn').hasClass('choosen_pro')){
                totalPriceNumber -=  parseFloat(price)
            }
        }
        $(this).siblings('.count_num').text(count)
        $(this).parent().siblings('p').text('￥' + (count*price).toFixed(2))
        showTotalPrice()
    })

    $('.cart_product_box').on('click','.add',function(event){         //增加商品数量
        event.stopPropagation()
        var price = getSinglePrice($(this).parents('.dada').index())
        var count = parseInt($(this).siblings('.count_num').text())
        count++
        if($(this).parents('.price_box').siblings('.products').find('.round_btn').hasClass('choosen_pro')){
            totalPriceNumber += parseFloat(price)
        }
        $(this).siblings('.count_num').text(count)
        $(this).parent().siblings('p').text('￥' + (count*price).toFixed(2))
        showTotalPrice()
    })

    $('.cart_product_box').on('click','.dada',function(){    //选中商品
        var panduan = true
        $('#all_select').removeClass('choosen_pro')
        var price = getSinglePrice($(this).index())
        var count = parseInt($(this).find('.count_num').text())
        if($(this).find('.round_btn').hasClass('choosen_pro')){
            $(this).find('.round_btn').removeClass('choosen_pro')
            totalPriceNumber -= (price * count)
        }else {
            $(this).find('.round_btn').addClass('choosen_pro')
            totalPriceNumber += (price * count)
        }
        showTotalPrice()
        $('.cart_product').each(function(index,item){
            if(!$(item).find('.round_btn').hasClass('choosen_pro')){
                panduan = false
            }
        })
        if(panduan){
            $('#all_select').addClass('choosen_pro')
        }
    })

    $('#all_select').on('click',function(){       //全选
        totalPriceNumber = 0;
        var judge = false;
        if($(this).hasClass('choosen_pro')){
            $(this).removeClass('choosen_pro')  
        }else {
            $(this).addClass('choosen_pro')
            judge = true
        }
        if(judge){
            $('.cart_product').each(function(index,item){
                $(item).find('.round_btn').addClass('choosen_pro')
                totalPriceNumber += parseFloat($(item).find('.price').text().substr(1))
            })
        }else {
            $('.cart_product').each(function(index,item){
                $(item).find('.round_btn').removeClass('choosen_pro')
                totalPriceNumber = 0
            }) 
        }
        showTotalPrice()
    })

    $('.jiesuan').on('click',function(){      //结算
        if(totalPriceNumber <= 0 ? 0 : totalPriceNumber.toFixed(2) > 0){
            console.log(1)     //ajax
            // console.log($('.cart_product').eq(1).find('.count_num').text())  这里修改dom 获取的是修改后的值  所以 有漏洞不能结算时从dom获取商品信息
        }else {
            return
        }
    })

    function showTotalPrice(){
        // console.log(totalPriceNumber)
        $('.account-btn span').text(totalPriceNumber <= 0 ? 0 : totalPriceNumber.toFixed(2))
        if((totalPriceNumber <= 0 ? 0 : totalPriceNumber.toFixed(2)) > 0){
            $('.jiesuan').css({backgroundColor:'#eb6100'})
        }else {
            $('.jiesuan').css({backgroundColor:'#a6a6a6'})
        }
    }

    function getSinglePrice(index){           //获取对应商品的单价
        return JSON.parse(sessionStorage.getItem('carts_data'))[index].productPrice
    }

    function spliceHTML(){      
        var txt = ''
        return function(arr){
            arr.forEach(function(item){
                txt += '<div class="dada">\
                            <div class="cart_product">\
                                <div class="top_shop_name">\
                                    <div class="round_btn"></div>\
                                    <p>' + item.shopName + '</p>\
                                </div>\
                                <div class="products">\
                                    <div class="round_btn"></div>\
                                    <img src="' + item.productImage + '" alt="">\
                                    <div class="product_content">\
                                        <p class="pro_name">' + item.productName + '</p>\
                                        <p class="guige">规格</p>\
                                    </div>\
                                </div>\
                                <div class="price_box">\
                                    <p class="price">￥' + ((item.productPrice)*(item.count)).toFixed(2) + '</p>\
                                    <div class="price-btn">\
                                        <span class="reduce">－</span>\
                                        <span class="count_num">' + item.count + '</span>\
                                        <span class="add">＋</span>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="del_box">\
                                <span class="shanchu">删除</span>\
                                <span class="quxiao">取消</span>\
                            </div>\
                        </div>'
            })
            $('.cart_product_box').empty().append(txt)
        }
    }

    $('.cart_product_box').on('touchstart','.cart_product',touchStart)
    $('.cart_product_box').on('touchmove','.cart_product',touchMove)
    $('.cart_product_box').on('click','.dada .quxiao',quxiao)
    $('.cart_product_box').on('click','.dada .shanchu',shanchu)

    function touchStart(e){
        startX = e.originalEvent.changedTouches[0].pageX
        startY = e.originalEvent.changedTouches[0].pageY;
    }

    function touchMove(e){          
        var moveEndX = e.originalEvent.changedTouches[0].pageX
        var moveEndY = e.originalEvent.changedTouches[0].pageY
        var X = moveEndX - startX
        var Y = moveEndY - startY
        if(Math.abs(X) > Math.abs(Y) && X > 0){              //右移
            e.preventDefault();
            $(this).removeClass('showDelBtn')
        }else if(Math.abs(X) > Math.abs(Y) && X < 0 ){          //左移
            e.preventDefault();
            $(this).addClass('showDelBtn')
        }else {
            return 
        }
    }

    function quxiao(){
        e.stopPropagation()
        $(this).parents('.del_box').siblings('.cart_product').removeClass('showDelBtn')
    }

    function shanchu(){
        e.stopPropagation()
        $(this).parents('.del_box').siblings('.cart_product').parent('.dada').remove()
    }

    function http(url,type,data){         //把ajax封装成promise
        var defer = Q.defer()
        if(type.toLowerCase() === 'get'){
            $.ajax({
                url:url,
                type:type,
                success:function(res){
                    defer.resolve(res)
                },
                error:function(err){
                    defer.reject(err)
                }
            })
        }else if(type.toLowerCase() === 'post'){
            $.ajax({
                url:url,
                type:type,
                data:data,
                success:function(res){
                    defer.resolve(res)
                },
                error:function(err){
                    defer.reject(err)
                }
            })
        }
        return defer.promise
    }

})()