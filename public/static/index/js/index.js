;
(function(){
    var mySwiper = new Swiper('.swiper-container', {
        autoplay: true,//可选选项，自动滑动
        loop : true //环路
    })


    http('goods/list','GET').then(data=>{
        spliceHTML()(data.result)
        return data.result
    }).then(data=>{
        judge(data)
    }).catch(err=>{
        console.log(err)
    })

    $('#search').on('click',function(){
        $('.search_message_box').show().siblings().not('.index_head').hide()
        $('.left-arrow').show()
        $('.cancel').show()
    })

    $('.cancel').on('click',function(){
        $('.search_message_box').hide().siblings('.product_container').show()
        $('.left-arrow').hide()
        $('.cancel').hide()
    })

    $('.area_title span').on('click',function(){
        var type = $(this).index()
        if(type === 0){
            $('.product_container').show().siblings('.other_menu_container').hide().siblings('.search_message_box').hide()
        }else {
            $('.product_container').hide().siblings('.other_menu_container').show().siblings('.search_message_box').hide()
            $('.other_product_box').eq(type - 1).css({display: 'flex'}).siblings().css({display: 'none'})
        }
        $(this).addClass('choosen_span').siblings().removeClass('choosen_span')
    })

    function judge(data){       //判断是否打折
        var arr = []
        data.forEach(function(item,index){
            if(!item.discount){
                arr.push(index)
            }
        })
        arr.forEach(function(item2){
            $('.youhui').eq(item2).hide()
        })
        return arr
    }

    function spliceHTML(){      
        var txt = ''
        return function(arr){
            arr.forEach(function(item){
                txt += '<a class="normal_product_list">\
                <img src="' + item.productImg + '" alt="">\
                <div class="right_product_intro">\
                    <p class="product_title">' + item.productName + '</p>\
                    <p class="product_favourable"><span class="youhui">优惠券</span><span class="baoyou">包邮</span></p>\
                    <p class="product_price"><em>￥' + item.price + '</em><span class="shouhuo">' + item.arrival + '人收货</span><span>' + item.address + '</span></p>\
                </div>\
            </a>'
            })
            $('.recommend-product-content-box').empty().append(txt)
        }
    }

    function spliceHTML_TWO(){      
        var txt = ''
        return function(arr){
            arr.forEach(function(item){
                txt += '<div class="other_product_single">\
                <img src="' + item.productImg + '" alt="">\
                <p>' + item.productName + '</p>\
                <span><em>￥</em>' + item.price + '</span>\
            </div>'
            })
            $('.other_product_box').empty().append(txt)
        }
    }

    function http(url,type,data){         //把ajax封装成promise
        var defer = Q.defer()
        if(type.toLowerCase() === 'get'){
            $.ajax({
                url:url,
                type:type,
                success:function(res){
                    defer.resolve(res)
                },
                error:function(err){
                    defer.reject(err)
                }
            })
        }else if(type.toLowerCase() === 'post'){
            $.ajax({
                url:url,
                type:type,
                data:data,
                success:function(res){
                    defer.resolve(res)
                },
                error:function(err){
                    defer.reject(err)
                }
            })
        }
        return defer.promise
    }


})()

    

